package com.master.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Movie implements Serializable {

    private final String name;
    private final String director;
    private final boolean christmasMovie;

    public Movie(@JsonProperty("name") String name,
                 @JsonProperty("director") String director,
                 @JsonProperty("christmasMovie") boolean christmasMovie) {
        this.name = name;
        this.director = director;
        this.christmasMovie = christmasMovie;
    }

    public String getName() {
        return name;
    }

    public String getDirector() {
        return director;
    }

    public boolean isChristmasMovie() {
        return christmasMovie;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "name='" + name + '\'' +
                ", director='" + director + '\'' +
                ", christmasMovie=" + christmasMovie +
                '}';
    }
}
