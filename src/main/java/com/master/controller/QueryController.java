package com.master.controller;

import com.master.domain.MovieResponse;
import com.master.service.BookSenderService;
import com.master.service.MovieException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import static com.master.utils.Constants.SETTINGS_ENDPOINT;

@RestController
public class QueryController {

    private Logger logger = LoggerFactory.getLogger(QueryController.class);

    @Autowired
    private BookSenderService bookSenderService;

    @RequestMapping(value = SETTINGS_ENDPOINT, method = RequestMethod.GET)
    public ResponseEntity<MovieResponse> retrieveAllPartnersSettings(@RequestParam String movieName) throws MovieException {
        logger.info("GET for targeted movie {}", movieName);
        MovieResponse movieResponse = bookSenderService.sendMessage(movieName);
        return ResponseEntity.ok().body(movieResponse);
    }
}
