package com.master.utils;

public class Constants {
    public static final String SETTINGS_ENDPOINT = "/settings";
    public static final String QUEUE_GENERIC_NAME = "queue_generic_name";
    public static final String QUEUE_SPECIFIC_NAME = "queue_specific_name";
    public static final String EXCHANGE_NAME= "exchange_name";
    public static final String ROUTING_KEY= "ROUTING_KEY";
}
