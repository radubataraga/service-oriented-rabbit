package com.master.service;

import com.master.domain.Movie;
import com.master.domain.MovieResponse;
import com.master.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookSenderService {

    private static final Logger log = LoggerFactory.getLogger(BookSenderService.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public MovieResponse sendMessage(String movieName) throws MovieException {
        final Movie message = new Movie(movieName, "not-Tarantino", true);
        log.info("Sending message to rabbit...");
        /*
        Instead of convertAndSend we use convertSendAndReceive which uses a callback queue
         Instead of creating for every rpc request a new callback quene, we should create only a callback quee for
         one client
         corellationID is used when for a single client a callback queue is allocated. A unique id will be generated
         for each clients request on this queue. Also matching request-response i done
         each client an exchange name and a routing key
        */
        Object movieRabbit = rabbitTemplate.convertSendAndReceive(Constants.EXCHANGE_NAME, Constants.ROUTING_KEY, message);
        log.info("Received message from rabbit........ {}", movieRabbit);
        if (movieRabbit instanceof MovieResponse) {
            MovieResponse movie = (MovieResponse) movieRabbit;
            if (movie.getTitle() == null) {
                log.info("Movie with name {} not found", movieName);
                throw new MovieException("Movie not found with name  " + movieName);
            }
            return movie;
        } else {
            MovieResponse movieResponse = new MovieResponse();
            movieResponse.setTitle(movieName);
            return movieResponse;
        }
    }
}
