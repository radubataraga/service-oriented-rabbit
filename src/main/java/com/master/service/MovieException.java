package com.master.service;

public class MovieException extends Throwable {
    public MovieException(String msg) {
        super(msg);
    }
}
